%{
	#include<stdio.h>
	#include<string.h>
	void infix_to_postfix(char*,char*);
	void infix_to_prefix(char*,char*);
	int top;
	char *stack,*temp;
%}

digit      [0-9]+
character  [a-zA-Z]
%%

\(*{digit}|{character}\)*([+\-*/]\(*{digit}|{character}\)*)+ {
					infix_to_prefix(yytext,temp);
					/* when only infix_to_postfix is asked, change the above line infix_to_postfix(yytext,temp) and change the function accordingly */
}
. ;

%%
int main(int argc,char* argv[]){
	yyin = fopen("a.txt","r");
	yylex();
	return 0;
}

void push(char c){
	stack[++top] = c;
}

char get_top(){
	return stack[top];
}

void pop(){
	--top;
}

int precedence(char c){
	if(c == '+' || c == '-')
		return 1;
	if(c == '*' || c == '/')
		return 2;
	return 3;
}

int is_operand(char c){
	return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'));
}

void reverse(char *s){
	int i=0;	
	int len = strlen(s);
	for(;i<len/2;i++){ 
		char temp = s[i];
		s[i] = s[len-i-1];
		s[len-i-1] = temp;
	}
}
void reverse_string_with_brackets(char *s){
	int i=0;	
	int len = strlen(s);
	char temp;
	for(;i<len/2;i++){ 
		char temp = s[len-i-1];
		if(s[i] == '('){
			s[len-i-1] = ')';

		}
		else if(s[i] == ')'){
			s[len-i-1] = '(';
		}
		else{
			s[len-i-1] = s[i];
		}
		if(temp == '('){
			s[i] = ')';
		}else if(temp == ')')
			s[i] = '(';
		else{
			s[i] = temp;
		}
		
	}
}
void infix_to_prefix(char *s,char *temp){

	int len = strlen(s);
	reverse_string_with_brackets(s);
	temp = (char *)malloc(len+1 * sizeof(char));
	infix_to_postfix(s,temp);
	reverse(temp);
	printf("\nInfix to Prefix is %s\n",temp);	
}
void infix_to_postfix(char *s,char*temp){
	int len = strlen(s);
	stack = (char *)malloc(len+1 * sizeof(char));
	
	//temp = (char *)malloc(len+1 * sizeof(char)); /*uncomment this line when only infix to postfix is asked*/

	top = -1;
	int i;
	int k = 0;
	for(i=0;i<len;i++){
		if(is_operand(s[i])){
			temp[k++] = s[i];	
		}
		else if(top == -1 || s[i] == '('|| get_top() == '(' || (s[i] != ')' && (precedence(s[i]) > precedence(get_top())))){
			push(s[i]);
		}else{
			if(s[i] == ')'){
				while(top != -1 && get_top() != '('){
					temp[k++] = get_top();
					pop();
				}
				pop();
			}else{
				while(top != -1 && (get_top() != '(') && (precedence(s[i]) <= precedence(get_top()))){
					temp[k++] = get_top();
					pop();	
				}	
				push(s[i]);
			}	
		}
	}
	while(top != -1){
		temp[k++] = get_top();
		pop();
	}	
	temp[k] = '\0';
	//printf("\nInfix to Postfix is %s\n",temp);/*Uncomment when infix to postfix is asked*/
}