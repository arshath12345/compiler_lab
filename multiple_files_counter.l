%{
    #include<stdio.h>
    /*Get multiple file names from
     command line and print
      word count,line count,character count in that files*/
    long wordCount = 0, lineCount = 0, characterCount = 0;
%}

word [^ \t\n]+
nextline \n

%%

{word} {wordCount++;characterCount += yyleng;}
{nextline} {characterCount++;lineCount++;}
. characterCount++;
%%

int main(int argc,char *argv[]){
    for(int i=1;i<argc;i++){ 
        yyin  = fopen(argv[i],"r");
        yylex();
        printf("%s\ncharacterCount:%lu\tWordCount:%lu\tLineCount:%lu\t\n",argv[i],characterCount,wordCount,lineCount);
        characterCount = 0;wordCount = 0;lineCount = 0;
    }
    return 0;
}