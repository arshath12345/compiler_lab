%{
#include<stdio.h>
#include<stdlib.h>    

double sym[26];

void yyerror(const char*);
int yylex();
%}

%union{
    double dval;
    int ival;
}

%token<ival>ICONST 
%token<dval>FCONST
%token<ival>NAME

%token PLUS MINUS MUL DIV
%token LBRAC RBRAC

%left PLUS MINUS
%left MUL DIV
%left LBRAC

%start program

%type<dval>expression

%%

program : program statement '\n'
        | ;

statement : NAME '=' expression { sym[$1] = $3;} ;
        | expression {printf("%lf\n",$1);} ;

expression: expression PLUS expression {$$ = $1 + $3;}
    |
            expression MINUS expression {$$ = $1 - $3;}
    |       
            expression MUL expression {$$ = $1 * $3;}
    |
            expression DIV expression {
                if($3 == 0)
                    yyerror("Divide by zero!");
                else
                    $$ = $1 / $3;
                printf(" / ");
            }
    | 
            LBRAC expression RBRAC {$$ = $2;}
    |       
            ICONST {$$ = $1;}
    |
            FCONST {$$ = $1;}
    |       
            NAME   {$$ = sym[$1];}


%%

#include"lex.yy.c"
int main(){
    return yyparse();
}
void yyerror(const char*s)
{
        fprintf(stderr,"%s\n",s);
}