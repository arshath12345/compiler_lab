%{
    #include<stdio.h>    
%}

digit [0-9]
int_num {digit}+
float_num {digit}*\.{digit}+

%%

{int_num} {yylval.ival = atoi(yytext);return ICONST;}
{float_num} {yylval.dval = atof(yytext);return FCONST;}
"+" {return PLUS;}
"-" {return MINUS;}
"*" {return MUL;}
"/" {return DIV;}
"(" {return LBRAC;}
")" {return RBRAC;}
[ \t]+ ;
"\n" {return *yytext;}
%%