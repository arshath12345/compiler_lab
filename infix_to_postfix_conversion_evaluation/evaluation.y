%{
#include<stdio.h>
#include<stdlib.h>    

void yyerror(const char*);
int yylex();
%}

%union{
    double dval;
    int ival;
}

%token<ival>ICONST 
%token<dval>FCONST
%token PLUS MINUS MUL DIV
%token LBRAC RBRAC

%left PLUS MINUS
%left MUL DIV
%left LBRAC

%start program

%type<dval>expression

%%

program : program statement
        | ;

statement : expression '\n'{printf("\n%lf is the answer\n",$1);}

expression: expression PLUS expression {$$ = $1 + $3;printf(" + ");}
    |
            expression MINUS expression {$$ = $1 - $3;printf(" - ");}
    |       
            expression MUL expression {$$ = $1 * $3;printf(" * ");}
    |
            expression DIV expression {
                if($3 == 0)
                    yyerror("Divide by zero!");
                else
                    $$ = $1 / $3;
                printf(" / ");
            }
    | 
            LBRAC expression RBRAC {$$ = $2;}
    |       
            ICONST {$$ = $1;printf(" %d ",$1);}
    |
            FCONST {$$ = $1;printf(" %lf ",$1);}


%%

#include"lex.yy.c"
int main(){
    return yyparse();
}
void yyerror(const char*s)
{
        fprintf(stderr,"%s\n",s);
}